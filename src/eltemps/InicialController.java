package eltemps;

import eltemps.domain.Weather;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class InicialController {

    private WeatherService weatherService = new WeatherService();

    @FXML
    private Label lbInfo;

    public void testJSON(ActionEvent actionEvent) {
        Weather current = weatherService.getCurrentWeather("Barcelona");

        if (current != null) {
            lbInfo.setText("Temp: " + current.getTemp() +
                    "\nFeels like: " + current.getFeelsLike() +
                    "\nMin: " + current.getMin() +
                    "\nMax: " + current.getMax() +
                    "\nPressure: " + current.getPressure() +
                    "\nHumidity: " + current.getHumidity());
        }
    }
}
